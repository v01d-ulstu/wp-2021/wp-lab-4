import urllib.parse

import requests
import bs4


def get_link_tree(url: str, seen=None, depth=1, max_depth=4):
    if depth > max_depth:
        return {}

    if not seen:
        seen = {url}

    link_tree = {}
    for sub_url in get_link_iter(url):
        if sub_url not in seen:
            seen.add(sub_url)
            link_tree[sub_url] = get_link_tree(sub_url, seen, depth + 1, max_depth)

    return link_tree


def get_link_iter(url: str):
    with requests.get(url) as res:
        html = res.text

    soup = bs4.BeautifulSoup(html, "html.parser")
    for html_tag_a in soup.find_all("a"):
        yield urllib.parse.urljoin(url, html_tag_a.get("href"))
