import flask

import config
import crawler


views = flask.Blueprint("views", __name__)


@views.route("/")
def index():
    return flask.send_from_directory(config.STATIC_FOLDER, "index.html")


@views.route("/tree")
def tree():
    try:
        url = flask.request.args["url"].strip()
        max_depth = int(flask.request.args.get("max_depth", config.DEFAULT_MAX_DEPTH))
    except (KeyError, ValueError):
        flask.abort(400)

    return flask.jsonify(crawler.get_link_tree(url, max_depth=max_depth))
