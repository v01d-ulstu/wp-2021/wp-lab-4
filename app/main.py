import flask

import config
import views


app = flask.Flask(__name__, static_folder=config.STATIC_FOLDER)
app.register_blueprint(views.views)
